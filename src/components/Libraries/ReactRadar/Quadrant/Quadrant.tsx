import React, {useContext} from 'react';
import Text from '../Text/Text';
import Path from '../Path/Path';
import Line from '../Line/Line';
import Item from '../Item/Item';
import {QuadrantWrapper} from './Quadrant.style';
import {ThemeContext} from '../theme-context';

interface Props {
    radiusDiminish: number;
    width: number;
    angle: number;
    rings: any[];
    transform: any;

    name: string;
    points: any[];
    index: number;
    rotateDegrees: number;
}

function Quadrant({
    radiusDiminish,
    width,
    angle,
    rings,
    transform,
    name,
    points,
    index,
    rotateDegrees
}: Props) {
    // context variables
    const {colorScale, quadrantsConfig: {textMargin, textYOffset, showOnlyFirstQuadrantLabels}} = useContext(ThemeContext);

    let ref = React.createRef();
    const ringWidth = width / 2;
    const radialAngle = 2 * Math.PI / 360 * angle;

    const onMouseOver = () => {
        // @ts-ignore
        ref.style.opacity = '1.0';
    };

    const onMouseOut = () => {
        // @ts-ignore
        ref.style.opacity = '0.7';
    };

    const onMouseClick = () => {
    // const svg = d3.select(ref);
    // svg.transition()
    //     .duration(2000)
    //     .style("transform", "translate(-300px, -300px) scale(" + 2 + ") ")
    };

    const calculateRadiusDiminish = (nrOfRings: any) => {
        let max = 1;

        // create the array. each value represents
        // the share of total radius among rings.
        let arr = [1];
        for (let i = 1; i < nrOfRings; i++) {
            max *= radiusDiminish;
            arr.push(max);
        }

        // calculate total shares of radius
        const sum = arr.reduce((a, b) => a + b);
        arr = arr.map((a) => a / sum);

        // now, each member of the array represent
        // the starting position of ring in the
        // circle
        arr.reverse();
        for (let i = 1; i < nrOfRings; i++) {
            // @ts-ignore
            arr[i] = arr[i - 1] + arr[i];
        }

        // add 0 for the center of the circle
        arr.push(0);

        // sort the array so that 0 is at the start
        arr.sort();

        return arr;
    };

    const radiuses = calculateRadiusDiminish(rings.length);

    return (
        <QuadrantWrapper
            transform={transform}
            onMouseOver={onMouseOver}
            onMouseOut={onMouseOut}
            onClick={onMouseClick}
            // @ts-ignore
            ref={(el) => ref = el}
        >

            <Line
                x2={ringWidth}
                y2={0}
                // @ts-ignore
                stroke={String(colorScale(index))}
            />

            {rings.map((ringValue: any, ringIndex: number) => {
                const ringsLength = rings.length;
                const title = ringIndex === rings.length - 1 ? name : undefined;

                // @ts-ignore
                const leftMargin = textMargin ?? (40 * (radiuses[ringIndex + 1] - (radiuses[ringIndex])));
                const showLabel = showOnlyFirstQuadrantLabels ? index === 0 : true;
                return (
                    <g key={`${index}-${ringIndex}`}>
                        {showLabel && (
                            <Text
                                name={ringValue}
                                // @ts-ignore
                                dx={leftMargin + (radiuses[ringIndex] * ringWidth)}
                                dy={textYOffset}
                            />
                        )}
                        <Path
                            quadIndex={index}
                            ringIndex={ringIndex}
                            ringWidth={ringWidth}
                            ringsLength={ringsLength}
                            quad_angle={radialAngle}
                            // @ts-ignore
                            outerRadius={radiuses[ringIndex + 1]}
                            // @ts-ignore
                            innerRadius={radiuses[ringIndex]}
                            title={title}
                        />
                    </g>
                );
            })}
            {points.map((value, index) => {
                return (
                    <Item
                        rotateDegrees={-rotateDegrees}
                        key={index}
                        data={value}
                    />
                );
            })}

        </QuadrantWrapper>
    );
}

export default Quadrant;
