import React, {useContext} from 'react';
import {rgb as d3rgb} from 'd3-color';
import {arc as d3arc} from 'd3-shape';
import {ThemeContext} from '../theme-context';

interface Props {
    quadIndex: number;
    ringIndex: number;
    ringWidth: number;
    ringsLength: number;
    quad_angle: number;
    outerRadius: number;
    innerRadius: number;
    title?: string;
}

function Path({
    quadIndex,
    ringIndex,
    ringWidth,
    ringsLength,
    quad_angle,
    outerRadius,
    innerRadius,
    title
}: Props) {
    // context variables
    const {fontSize, fontFamily, colorScale} = useContext(ThemeContext);

    //@ts-ignore
    const rgb = d3rgb(colorScale(quadIndex));
    const fill = rgb.brighter(ringIndex / ringsLength * 0.9);
    const uniquePathId = `${quadIndex}-${ringIndex}`;

    const archFunction = () => {
        return d3arc()
            .outerRadius(() => {
                return outerRadius * ringWidth;
            })
            .innerRadius(() => {
                return innerRadius * ringWidth;
            })
            .startAngle(() => {
                return Math.PI / 2;
            })
            .endAngle(() => {
                return quad_angle + Math.PI / 2;
            });
    };

    return (
        <g>
            <path
                id={uniquePathId}
                className="quadrant"
                //@ts-ignore
                d={archFunction()()}
                //@ts-ignore
                fill={fill}
            />

            {title
                && (
                    <text
                        dx={ringWidth / 2}
                        fontSize={fontSize}
                        fontFamily={fontFamily}
                    >
                        <textPath href={`#${uniquePathId}`}>
                            {title}
                        </textPath>
                    </text>
                )}
        </g>
    );
}

export default Path;
