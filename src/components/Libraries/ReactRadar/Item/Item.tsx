import React, {useState, useContext, useRef} from 'react';
import {ItemWrapper} from './Item.style';
import {ThemeContext} from '../theme-context';

const MAX_LENGTH = 15;

interface Props {
    rotateDegrees: number;
    data: {
        name: string;
        x: number;
        y: number;
        id: number;
    };
}

function Item({data, rotateDegrees}: Props) {
    // create ref
    const ref = useRef(null);

    // context variables
    const {itemFontSize, fontFamily} = useContext(ThemeContext);

    // state variables
    const [isHovered, setIsHovered] = useState(false);

    const shortName = data.name.length > MAX_LENGTH
        ? `${data.name.substr(0, MAX_LENGTH)}...`
        : data.name;

    const onMouseToggle = () => {
        setIsHovered(!isHovered);
    };

    return (
        <ItemWrapper
            className="blip"
            id={`blip-${data.id}`}
            transform={` rotate(${rotateDegrees}) translate(${data.x},${data.y})`}
            onMouseEnter={onMouseToggle}
            onMouseLeave={onMouseToggle}
            ref={ref}
            style={{
                opacity: isHovered ? '1.0' : '0.7',
                fontWeight: isHovered ? 'Bold' : 'Normal'
            }}
        >
            <circle r="4px" />
            <text
                className="name"
                dx="7px"
                dy="7px"
                fontSize={itemFontSize}
                fontFamily={fontFamily}
            >
                {isHovered ? data.name : shortName}
            </text>
        </ItemWrapper>
    );
}

export default Item;
