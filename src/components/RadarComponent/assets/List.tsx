import React, {useState} from 'react';
import ListItem from './ListItem';

interface Props {
    data: any[];
}

export default function List({data}: Props) {
    const [elements, setActiveItem] = useState<any[]>(data.map((item, index) => {
        return {item, active: index === 0};
    }));
    const changeActiveListItem = (e: React.ChangeEvent<any>, value: boolean) => {
        setActiveItem(elements.map((element) => {
            //@ts-ignore
            if (e.innerHTML === element.item.name) {
                return {item: element.item, active: value};
            } else {
                return {item: element.item, active: false};
            }
        }));
    };

    return (
        <div className="radar__list">
            {
                elements.map((element, index) => (
                    <ListItem
                        {...element}
                        onChangeState={changeActiveListItem}
                        key={element.name + String(index)}
                    />
                ))
            }
        </div>
    );
}
