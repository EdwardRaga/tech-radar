export default function ListHandler(item: any) {
    const elements = document.querySelectorAll('.name');

    elements.forEach((element) => {
        element.classList.remove('activeLabel');
        element.parentElement?.parentElement?.classList.remove('activePart');
    });
    elements.forEach((element) => {
        if (element.innerHTML.includes(item.name)) {
            element.classList.add('activeLabel');
            element.parentElement?.parentElement?.classList.add('activePart');
        }
    });
}
